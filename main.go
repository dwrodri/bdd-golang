package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//IsBranchOp indicates whether a RISC-V Opcode is is a jump/branch
func IsBranchOp(opcode string) bool {
	return strings.ContainsAny(opcode, "jb")
}

//WasBranchTaken checks if the PC was changed to something besides the next instruction
func WasBranchTaken(branch_src_pc uint64, opcode string, next_pc uint64) bool {
	offset := next_pc - branch_src_pc
	if offset == 2 && opcode[:2] == "c." {
		return false
	} else if offset == 4 && opcode[:2] != "c." {
		return false
	}
	return true
}

func main() {
	fd, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(fd)
	scanner.Split(bufio.ScanLines)
	branchOpcode := ""
	branchSrcPc := uint64(0)
	determineBranchOutcome := false
	for line := ""; scanner.Scan(); {
		line = scanner.Text()
		elems := strings.Split(line, " ")
		if elems[0][:2] != "0x" {
			continue
		}
		if determineBranchOutcome {
			new_pc, err := strconv.ParseUint(elems[0][2:], 16, 64)
			if err != nil {
				panic(err)
			}
			if WasBranchTaken(branchSrcPc, branchOpcode, new_pc) {
				fmt.Printf(" TAKEN\n")
			} else {
				fmt.Printf(" NOT TAKEN\n")
			}
			determineBranchOutcome = false
		}
		if IsBranchOp(elems[1]) {
			fmt.Printf("%s  BRANCH", elems[0])
			branchOpcode = elems[1]
			branchSrcPc, _ = strconv.ParseUint(elems[0][2:], 16, 64)
			determineBranchOutcome = true
		} else {
			fmt.Printf("%s  NOT BRANCH\n", elems[0])
		}
	}

}
