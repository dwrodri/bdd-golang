package tage

import "math"

const (
	NumTables uint64 = 6
)

// Golang doesn't support const tables so I have to politely ask you to treat this as const
var TageParams = [NumTables][3]uint64{
	{128, 2, 7},
	{128, 4, 7},
	{256, 8, 8},
	{256, 16, 8},
	{128, 32, 9},
	{128, 64, 9},
}

type TageTable struct {
	HistorySize   uint8
	TagMask       uint16
	UsefulCounter []uint8
	Tag           []uint16
	Counter       []uint8
}

type TagePredictor struct {
	GlobalHistory    uint64
	FallbackCounters [128]uint8
	Tables           [NumTables]TageTable
}

func NewTagePredictor() TagePredictor {
	t := TagePredictor{}
	for i, table := range t.Tables {
		table.HistorySize = uint8(TageParams[i][1])
		table.TagMask = (1 << (TageParams[i][2] + 1)) - 1
		table.UsefulCounter = make([]uint8, TageParams[i][0])
		table.Tag = make([]uint16, TageParams[i][0])
		table.Counter = make([]uint8, TageParams[i][0])
	}
	return t
}

// calculateTagAndHash provides the necessary key and value for accessing the table
func (t TageTable) calculateTagAndHash(pc, history uint64) (uint64, uint64) {
	numIdxBits := uint64(math.Ceil(math.Log2(float64(len(t.Counter)))))
	numTagBits := uint64(math.Ceil(math.Log2(float64(len(t.Counter)))))
	idxMask := (1 << (numIdxBits + 1)) - 1
	foldedHist := uint64(0)
	for temp := history; temp > 0; temp >>= numIdxBits {
		foldedHist = foldedHist ^ (temp & uint64(idxMask))
	}
	idx := (pc & uint64(idxMask)) ^ foldedHist
	foldedHist = 0
	for temp := history; temp > 0; temp >>= numTagBits {
		foldedHist = foldedHist ^ (temp & uint64(t.TagMask))
	}
	tag := (pc >> numIdxBits) ^ foldedHist
	return tag, idx
}

// Access queries the table and updates the counters accordingly
func (t *TageTable) Access(idx uint64) uint8 {
	return t.Counter[idx]
}

func (p *TagePredictor) ProcessPrediction(pc uint64, actualOutcome bool) bool {
	// access fallback table
	fallbackIdx := pc & 0x0fff
	fallbackPred := p.FallbackCounters[fallbackIdx]&4 == 4

	// find a tag match
	bestMatchIdx := -1
	bestMatchPred := false
	for tableIdx, table := range p.Tables {
		tag, hash := table.calculateTagAndHash(pc, p.GlobalHistory)
		if table.Tag[tag] == uint16(hash) && tableIdx > bestMatchIdx {
			bestMatchIdx = tableIdx
			bestMatchPred = int(table.Access(tag))&0x4 == 0x4
		}
	}

	// update fallback counter
	if fallbackPred == actualOutcome {
		if actualOutcome && p.FallbackCounters[fallbackIdx] < 7 {
			p.FallbackCounters[fallbackIdx]++
		} else if !actualOutcome && p.FallbackCounters[fallbackIdx] > 0 {
			p.FallbackCounters[fallbackIdx]--
		}
	}

	return fallbackPred || bestMatchPred

}
